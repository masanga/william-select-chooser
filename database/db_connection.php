<?php

// Written By : Elijah E. Masanga
// 
// Database Connection
// 
// I am returning the connection in a function
// to help me reuse it after I use require_once
// in other files

function db() {

	$server = "127.0.0.1";
	$user = "root";
	$pass = "";

	$db_name = "william_bus_chooser";

	// Connection to the MySQL Database
	$conn = new mysqli($server, $user, $pass, $db_name);
	
	if ($conn->connect_error) {
	  die("Connection failed: " . $conn->connect_error);
	} 

	return $conn;
}