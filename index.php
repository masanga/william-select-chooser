<?php

//Choosing Buses from the database

require_once ("database/db_connection.php");

$sql = "SELECT * FROM `buses` WHERE 1";
$result = db()->query($sql);

?>

<!DOCTYPE html>
<html>
<head>
	<title>William Bus Chooser</title>
	<script type="text/javascript" src="styles/sweetalert.js"></script>
	<link rel="stylesheet" href="styles/app.css">
</head>
<body>
	<br/><br/>
	<div class="body-centered">

	<h2>Kata Tiketi Sasa</h2>
	<hr/>

	<form action="#" method="POST" onsubmit="processForm(); return false;">
		<table>
			<tr>
				<td>Your First Name</td>
				<td><input type="text" name="firstName" class="william-input-box"></td>
			</tr>

			<tr>
				<td>Your Last Name</td>
				<td><input type="text" name="firstName" class="william-input-box"></td>
			</tr>

			<tr>
				<td><strong>Choosing</strong></td>
				<td>
					<select class="william-input-box">
						<?php
						if ($result->num_rows > 0) {
						  while($row = $result->fetch_assoc()) {
						  	echo "<option value='".$row['id']."'>".$row['bus_name']."</option>";
						  }
						}
						?>
					</select>
				</td>
			</tr>

			<tr>
				<td></td>
				<td><input type="submit" name="firstName" class="willian-input-button"></td>
			</tr>
		</table>
	</form>

	<script type="text/javascript">
		//https://sweetalert2.github.io/
		function processForm() 
		{
			Swal.fire(
			  'Good job!',
			  'You clicked the button!',
			  'success'
			)
		}
	</script>

	</div>
</body>
</html>
